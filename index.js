const request = require('request-promise-native')

module.exports = class {
  constructor (baseUri) {
    this.baseUri = baseUri
  }

  parseDataElements (o) {
    // parse the dataElements into usable JSON
    const dataElementsJson = {}
    o.dataElements.forEach(d => {
      switch(d.type) {
        case "boolean": {
          dataElementsJson[d.key] = d.value === 'true'
          break
        }
        case "double": {
          dataElementsJson[d.key] = parseFloat(d.value)
          break
        }
        case "integer": {
          dataElementsJson[d.key] = parseInt(d.value)
          break
        }
        default: {
          dataElementsJson[d.key] = d.value
          break
        }
      }
    })
    o.dataElements = dataElementsJson
    // return the object, for convenience
    return o
  }

  revertDataElements (o) {
    // revert normal JSON dataElements into REST service JSON
    const modified = []
    const keys = Object.keys(o.dataElements)
    for (const key of keys) {
      const value = o.dataElements[key]
      modified.push({
          key,
          value: String(value),
          type: typeof value
      })
    }
    o.dataElements = modified
    // return the object, for convenience
    return o
  }

  async search (type, query, operation = 'or') {
    const uri = `${this.baseUri}/search/${type}`
    // determine if query passed was object or something else
    if (typeof query === 'string') {
      query = {
        'query_string': [query]
      }
    }
    const body = {
      operation,
      query
    }

    const options = {
      method: 'POST',
      uri,
      body,
      json: true
    }

    try {
      const response = await request(options)
      // just return empty results
      if (response.length === 0) return response
      // format data elements on each object returned
      response.forEach(o => {
        this.parseDataElements(o)
      })
      // return modified response
      return response
    } catch (e) {
      throw e
    }
  }

  async status () {
    const uri = `${this.baseUri}/status`

    const options = {
      method: 'GET',
      uri,
      json: true
    }

    try {
      return await request(options)
    } catch (e) {
      throw e
    }
  }

  async get (type, id) {
    const uri = `${this.baseUri}/${type}/${id}`

    const options = {
      method: 'GET',
      uri,
      json: true
    }

    try {
      const response = await request(options)
      // just return empty results
      if (response.length === 0) return response
      // othewise, parse the data elements before returning
      return this.parseDataElements(response)
    } catch (e) {
      throw e
    }
  }

  async delete (type, id) {
    const uri = `${this.baseUri}/${type}/${id}`

    const options = {
      method: 'DELETE',
      uri,
      json: true
    }

    try {
      return await request(options)
    } catch (e) {
      throw e
    }
  }

  async update (type, id, body) {
    const uri = `${this.baseUri}/${type}/${id}`
    // format body
    this.revertDataElements(body)

    const options = {
      method: 'PUT',
      uri,
      body,
      json: true
    }

    try {
      return await request(options)
    } catch (e) {
      throw e
    }
  }

  async create (body) {
    const uri = `${this.baseUri}/`
    // format body
    this.revertDataElements(body)

    const options = {
      method: 'POST',
      uri,
      body,
      json: true,
      resolveWithFullResponse: true
    }

    try {
      const response = await request(options)
      // return the new ID
      return response.headers.location.split('/').pop()
    } catch (e) {
      throw e
    }
  }
}
