const CS = require('../index')
const cs = new CS(`http://localhost:8080/rest`)

async function run () {
  try {
    // get REST service status
    const status = await cs.status()
    console.log('status', status)

    // create customer
    const newCustomer = {
      "type": "customer",
      "fieldsets": [ "cisco.base.customer" ],
      "dataElements": {
        "Context_First_Name": "Jimothy",
        "Context_Last_Name": "Smith"
      }
    }
    const id = await cs.create(newCustomer)


    // search customer using query object
    const searchResults1 = await cs.search('customer', {
      Context_First_Name : ['Jimothy'],
      Context_Last_Name: ['Smith']
    }, 'and')
    console.log('search results 1:', searchResults1)

    // seach customer using query_string
    const searchResults2 = await cs.search('customer', 'Jimothy')
    console.log('search results 2:', searchResults2)

    // get new customer details
    const customer = await cs.get('customer', id)
    console.log('original customer: ', customer)

    // update customer data
    customer.dataElements.Context_First_Name = "Zeek"
    customer.dataElements.Context_Last_Name = "Deek"
    // update customer on CS
    await cs.update('customer', id, customer)

    // get updated customer info and log
    const updated = await cs.get('customer', id)
    console.log('updated customer: ', updated)

    // delete new customer
    await cs.delete('customer', id)
    console.log('deleted customer: ', id)
  }  catch (e) {
    console.log(e.message)
  }
}
run ()
